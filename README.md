### E.COSPACE

# Présentation
Le projet est le suivant :
E.COSPACE est le nom d’une nouvelle entreprise qui souhaite s’investir dès à présent afin de pouvoir proposer, lorsque le voyage dans l’espace sera ouvert au grand public, une solution qui a pour but de diminuer l’utilisation de combustibles ainsi que de réduire la pollution aussi bien sur la planète Terre que tout autre endroit.
Le projet est en ligne à cette adresse : https://vfrancoi.lpmiaw.univ-lr.fr/e-cospace/

# Techniques choisies
- Grid et flex sur different élément
- La propriété css **transform: skewX( ... );** pour créer les formes souhaitées des inputs et résultats de recherche. Cette propriété permet d'étirer chaque point de l'élément d'un certain angle dans la direction horizontale.
- La propriété css **clip-path: polygon( ... );** pour créer les formes souhaitééd de l'image de la voie lactée. Cette propriété permet de rogner une partie d'un element.
- Utilisation de LeaderLine (https://anseki.github.io/leader-line/) pour la création des flèches aussi bien droite qu'incurvée sur les pages.

# Principales difficultés rencontrées
- Les formes originales des inputs, de l'image de la voie lactée et des résultats de recherche.
    Solutions : - Propriété css **transform: skewX( ... );** pour les inputs et résultats de recherche.
                - Propriété css **clip-path: polygon( ... );** pour l'image de la voie lactée.
- Les flèches présentes dans les pages.
    Solution : Utilisation de LeaderLine : https://anseki.github.io/leader-line/
