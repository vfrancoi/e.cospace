if (innerWidth > 992) {
    new LeaderLine(
        LeaderLine.pointAnchor(document.querySelector('.search__label--starting'), { x: '98%', y: '50%' }),
        LeaderLine.pointAnchor(document.querySelector('.search__label--destination'), { x: '2%', y: '50%' }),
        {
            startSocket: 'right',
            endSocket: 'left',
            color: '#FEFBFF',
            dash: true,
            size: 2,
            endPlug: 'arrow2',
            startSocketGravity: [450, 150],
            endSocketGravity: [-200, -250]
        }
    );

    new LeaderLine(
        document.querySelector('.search__label--destination'),
        document.querySelector('.search__label--date'),
        {
            startSocket: 'bottom',
            endSocket: 'bottom',
            color: '#FEFBFF',
            dash: true,
            size: 2,
            endPlug: 'arrow2',
            startSocketGravity: [10, 30],
            endSocketGravity: [-10, 50]
        }
    );

    new LeaderLine(
        LeaderLine.pointAnchor(document.querySelector('.search__label--date'), { x: '98%', y: '50%' }),
        LeaderLine.pointAnchor(document.querySelector('.search__label--person'), { x: '2%', y: '50%' }),
        {
            startSocket: 'right',
            endSocket: 'left',
            color: '#FEFBFF',
            dash: true,
            size: 2,
            endPlug: 'arrow2'
        }
    );

    new LeaderLine(
        LeaderLine.pointAnchor(document.querySelector('.search__label--person'), { x: '98%', y: '50%' }),
        LeaderLine.pointAnchor(document.querySelector('.search__label--search'), { x: '2%', y: '50%' }),
        {
            startSocket: 'right',
            endSocket: 'left',
            color: '#FEFBFF',
            dash: true,
            size: 2,
            endPlug: 'arrow2'
        }
    );
}
else {
    new LeaderLine(
        LeaderLine.pointAnchor(document.querySelector('.search__label--starting'), { x: '98%', y: '75%' }),
        LeaderLine.pointAnchor(document.querySelector('.search__label--destination'), { x: '75%', y: '0%' }),
        {
            startSocket: 'right',
            endSocket: 'top',
            color: '#FEFBFF',
            dash: true,
            size: 2,
            endPlug: 'arrow2',
            startSocketGravity: [30, 175],
            endSocketGravity: [50, -175]
        }
    );

    new LeaderLine(
        document.querySelector('.search__label--destination'),
        LeaderLine.pointAnchor(document.querySelector('.search__label--date'), { x: '98%', y: '50%' }),
        {
            startSocket: 'top',
            endSocket: 'right',
            color: '#FEFBFF',
            dash: true,
            size: 2,
            endPlug: 'arrow2'
        }
    );

    new LeaderLine(
        document.querySelector('.search__label--date'),
        LeaderLine.pointAnchor(document.querySelector('.search__label--person'), { x: '2%', y: '50%' }),
        {
            startSocket: 'bottom',
            endSocket: 'left',
            color: '#FEFBFF',
            dash: true,
            size: 2,
            endPlug: 'arrow2',
            startSocketGravity: [0, 50],
            endSocketGravity: [-200, -10]
        }
    );

    new LeaderLine(
        document.querySelector('.search__label--person'),
        LeaderLine.pointAnchor(document.querySelector('.search__label--search'), { x: '98%', y: '50%' }),
        {
            startSocket: 'bottom',
            endSocket: 'right',
            color: '#FEFBFF',
            dash: true,
            size: 2,
            endPlug: 'arrow2'
        }
    );
}