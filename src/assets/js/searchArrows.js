const resultsCorrespondences = document.getElementsByClassName('result__correspondences');
Array.prototype.forEach.call(resultsCorrespondences, resultCorrespondences => {
    resultCorrespondences = [...resultCorrespondences.children];
    abstract = resultCorrespondences.shift();
    resultCorrespondences.forEach(element => {
        new LeaderLine(
            LeaderLine.pointAnchor(abstract, { x: '4%', y: '100%' }),
            element,
            {
                path: 'grid',
                endSocket: 'left',
                color: '#FEFBFF',
                size: 3,
                endPlug: 'arrow2'
            }
        );
    });
});

new LeaderLine(
    document.querySelector('.search-information__starting'),
    document.querySelector('.search-information__destination'),
    {
        path: 'grid',
        color: '#FEFBFF',
        size: 3,
        endPlug: 'arrow2'
    }
);